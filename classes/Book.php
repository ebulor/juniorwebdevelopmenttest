<?php

interface HavingWeight{
    public function setWeight($weight);
   
}

class Book extends Product implements HavingWeight{
       
    public function setWeight($weight){
        $this->weight = $weight;
    }
    
    
}

