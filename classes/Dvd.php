<?php
interface HavingSize{
    public function setSize($size);
}

class Dvd extends Product implements HavingSize{    
    public function setSize($size){
        $this->size = $size;
    }
    
}
