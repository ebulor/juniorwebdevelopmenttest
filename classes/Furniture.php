<?php

interface HavingFur_dimensions{
    public function setHeight($height);
    public function setWidth($width);
    public function setLength($length);
}

class Furniture extends Product implements HavingFur_dimensions{
    public function setHeight($height){
        $this->height = $height;
    }
    public function setWidth($width){
        $this->width = $width;
    }
    public function setLength($length){
        $this->length = $length;
    }
}

