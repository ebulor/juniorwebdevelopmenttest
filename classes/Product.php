<?php
include 'classes/Book.php';
include 'classes/Dvd.php';
include 'classes/Furniture.php';

abstract class Product extends Dbh {
    public $sku;
    public $name;
    public $price;
    protected $size;
    protected $weight;
    protected $height;
    protected $width;
    protected $length;

    public function setSku($sku){
        $this->sku = $sku;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function setPrice($price){
        $this->price = $price;
    }        

    public function insertProduct(){    
        $sql = "INSERT INTO product(sku, name, price, size, width, height, length, weight)
        VALUES(:sku, :name, :price, :size, :width, :height, :length, :weight)";

        $stmt = $this->connect()->prepare($sql);
        $stmt->bindParam(':sku', $this->sku);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':price', $this->price);
        $stmt->bindParam(':size', $this->size);        
        $stmt->bindParam(':width', $this->width);
        $stmt->bindParam(':height', $this->height);
        $stmt->bindParam(':length', $this->length);
        $stmt->bindParam(':weight', $this->weight);
        return $stmt->execute();
        
    }

    public function readAll()
    {
        $sql = "SELECT * FROM product";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
        
    public function delete(array $id) {
        $placeholders = trim(str_repeat('?,', count($id)), ',');
        $sql = "DELETE FROM product WHERE product_id IN ($placeholders)";
        $stmt = $this->connect()->prepare($sql);
        return $stmt->execute($id);
    }

    public function displayProducts(){
        $sql = "SELECT * FROM product";       
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        
        while($row = $stmt->fetch()){            
              echo'
              <div class="display-group">';
              echo'
              <div class="checkbox">';
               echo '<input value="'.$row['product_id'].'" type="checkbox" name="id[]" class="delete-checkbox"> ';                   
               echo '</div>';   
               echo '<p>'.$row['sku'] .'</p>';
                echo '<p>'.$row['name'] .'</p>';
               echo '<p>'.$row['price'] ."$".'</p>';
               if (!empty($row["size"])){echo "<p>Size: ".$row['size']." CM"."</p>";}
               if (!empty($row["weight"])){echo "<p>Weight: ".$row['weight']." KG"."</p>";}
               if (!empty($row["height"])&& !empty($row["width"])&& !empty($row["length"])){
                   echo "<p>Dimension: ". $row['height'] ."x". $row['width'] ."x". $row['length'] ."</p>"
                ;}           
                echo '</div>';              
            }         
    }
}