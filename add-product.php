<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/product-style.css">   
    <title>Junior web development test</title>
</head>
<body>
    <main>                
        <form action="product-view.php" method="POST" id="product_form" class="product_form">
            <div class="header">
                <p>Product Add</p>
                <div class="btns">
                    <button type="submit" name="submitBtn" class="save-btn">Save</button>
                    <a class="cancel-btn" href="index.php">Cancel</a>
                </div>
            </div>  
            <div class="form-input">
                <span class="error-msg"></span>     
                <div class="form-group">
                <label for="sku">SKU</label>
                <input type="text" name="sku" id="sku">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name">
                </div>
                <div class="form-group">
                    <label for="price">Price ($)</label>       
                    <input type="number" name="price" id="price">
                </div>
                <div class="form-group">
                    <label for="productType">Type Switcher</label>
                    <select name="productType" id="productType" >
                        <option value=""></option>
                        <option value="dvd" >DVD</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                    </select>
                </div>  
                <span class="description"></span> 
                <div id="bookForm">
                    <div class="form-group">
                        <label for="weight">Weight (KG)</label>       
                        <input type="number" name="weight" id="weight">
                    </div>
                </div> 
                <div id="dvdForm">
                    <div class="form-group">
                        <label for="size">Size (MB)</label>       
                        <input type="number" name="size" id="size">
                    </div>
                </div>
                <div id="furnitureForm">                    
                    <div class="form-group">
                        <label for="height">Height (CM)</label>       
                        <input type="number" name="height" id="height">
                    </div>
                    <div class="form-group">
                        <label for="width">Width (CM)</label>       
                        <input type="number" name="width" id="width">
                    </div>  
                    <div class="form-group">
                        <label for="length">Length (CM)</label>       
                        <input type="number" name="length" id="length">
                    </div>
                </div>  
            </div>                              
        </form>
    </main>


    <script src="app.js"></script>
</body>
</html>