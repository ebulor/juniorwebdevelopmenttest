const inputField = document.querySelectorAll(".product_form input");
const dvdForm = document.getElementById("dvdForm");
const bookForm = document.getElementById("bookForm");
const furnitureForm = document.getElementById("furnitureForm");
const form  = document.getElementById("product_form")
const description = document.querySelector(".description")
const select = document.getElementById("productType")
const errorMsg = document.querySelector(".error-msg");


select.addEventListener("change", () =>{
    if(select.value === "dvd"){
        description.style.display = "block";
        description.textContent = "Please, provide size";
        dvdForm.classList.add("show")
        bookForm.classList.remove("show")
        furnitureForm.classList.remove("show")
    }
    else if(select.value === "book"){
        description.style.display = "block";
        description.textContent = "Please, provide weight";    
        bookForm.classList.add("show")
        furnitureForm.classList.remove("show")
        dvdForm.classList.remove("show")
    }
    else if(select.value === "furniture"){
        description.style.display = "block";
        description.textContent = "Please, provide dimensions in HxWxL format";  
        furnitureForm.classList.add("show")
        bookForm.classList.remove("show")   
        dvdForm.classList.remove("show")
    }
})


const saveBtn = document.querySelector(".save-btn")
saveBtn.addEventListener("click", (e)=>{
    inputField.forEach((input)=>{
    const inputParent = input.parentElement;
   
    if (window.getComputedStyle(inputParent).display !== "none"){
        if(input.value === "" || select.value === '') {
            errorMsg.style.display = "block"
            errorMsg.innerHTML = "* Please, submit required data"
            e.preventDefault();
           return
        }
        else{
            errorMsg.style.display = "none"
            return true;           
             
        }
    }
    
  })      
      
}) 
