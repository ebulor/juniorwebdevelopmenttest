<?php
include 'classes/Dbh.php';
include 'classes/Product.php';  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/main.css" />
    <title>Junior web development test</title>
  </head>
  <body>
    <main>
      <form action="product-view.php"  method="POST">
        <div class="header">
          <h1>Product List</h1>
          <div class="btns">          
              <a class="add-btn" href="add-product.php">ADD</a>  
              <button type="submit" id="delete-product-btn" name="deleteBtn">MASS DELETE</button>          
             
          </div>
        </div>                   
          <div class="display"> 
            <?php             
              $book = new Book();
              $dvd = new Dvd();
              $furniture = new Furniture();
            ?> 
             <?php              
              if($book->readAll() > 0){
                $book->displayProducts();
              }
              else if ($dvd->readAll() > 0){
                $dvd->displayProducts();
              }
              else if ($furniture->readAll() > 0){
                $furniture->displayProducts();
              }             
            ?>           
          </div>
      </form>
    </main>
  <footer>
    <p>Scandiweb Test assignment</p>
  </footer>
    
  </body>
</html>
