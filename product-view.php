<?php
include 'index.php'; 

class Product_View extends Product{
    public function productView(){
        $book = new Book();
        $dvd = new Dvd();
        $furniture = new Furniture();

        if(isset($_POST["submitBtn"])){   
           
            $sku = $_POST["sku"];
            $name = $_POST["name"];
            $price = $_POST["price"];
            $size = isset($_POST['size']) ? $_POST['size'] : ''; 
            $weight = isset($_POST['weight']) ? $_POST['weight'] : '';
            $height = isset($_POST['height']) ? $_POST['height'] : '';
            $width = isset($_POST['width']) ? $_POST['width'] : '';
            $length = isset($_POST['length']) ? $_POST['length'] : '';         

            $prod_type = $_POST["productType"];  
            if($prod_type == "book"){
                $book->setSku($sku);    
                $book->setName($name);    
                $book->setPrice($price); 
                $book->setWeight($weight);  
                $book->insertProduct();               
            }
            else if($prod_type == "dvd"){  
                $dvd->setSku($sku);    
                $dvd->setName($name);    
                $dvd->setPrice($price);
                $dvd->setSize($size);
                $dvd->insertProduct();               
            }
            else if($prod_type == "furniture"){            
                $furniture->setSku($sku);    
                $furniture->setName($name);    
                $furniture->setPrice($price);           
                $furniture->setHeight($height);    
                $furniture->setWidth($width);    
                $furniture->setLength($length);
                $furniture->insertProduct();               
            }
            
            header('Location:index.php');    
        }


        if (isset($_POST['deleteBtn'])) {
            header('Location:index.php');
            $id = $_POST['id'];
            $book->delete($id);
            $dvd->delete($id);
            $furniture->delete($id);
                
        }
    }
}

$product = new Product_View();
$product->productView();